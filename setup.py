from distutils.core import setup

setup(
    name='corpora_reader',
    version='1.2',
    packages=['corpora_reader_gui'],
    package_dir={'': 'corpora_reader_gui'},
    url='',
    license='GLK',
    author='Krzysztof Dorosz',
    author_email='cypreess@gmail.com',
    description='Corpora format Web reader',
    scripts=['scripts/corpora_reader.py'],
    install_requires=['django', 'fapws3', 'corpora'],

)
