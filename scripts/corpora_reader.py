#!/usr/bin/env python
import argparse
import sys
from corpora.corpus import Corpus
from django.core import management
import os
import fapws._evwsgi as evwsgi
from fapws import base
from fapws.contrib import django_handler, log
from fapws.contrib import views

os.environ['DJANGO_SETTINGS_MODULE'] = 'corpora_reader_gui.settings'

parser = argparse.ArgumentParser(description='Starts Corpora Reader Web GUI.')
parser.add_argument( '--port', type=int, default=8000, help="defaults to 8000")
parser.add_argument( '--host', default='127.0.0.1', help="defaults to 127.0.0.1")
parser.add_argument('corpus_path')
args = parser.parse_args()

try:
    c = Corpus(args.corpus_path)
except:
    parser.print_usage()
    print >> sys.stderr, "corpus_reader.py: error: Path does not point to valid corpus"
    sys.exit(1)

del c

os.environ['CORPUS_PATH'] = args.corpus_path
from corpora_reader_gui.settings import READER_INDEX_FILENAME

if not os.path.exists( os.path.join(args.corpus_path, READER_INDEX_FILENAME)):
    print >> sys.stderr, "Indexing corpus for GUI"
    management.call_command('reader_index', args.corpus_path)#,  verbosity=0, interactive=False)

sys.setcheckinterval=100000 # since we don't use threads, internal checks are no more required
evwsgi.start(args.host, str(args.port))
evwsgi.set_base_module(base)

@log.Log()
def generic(environ, start_response):
    res=django_handler.handler(environ, start_response)
    return [res]

staticfile = views.Staticfile(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../corpora_reader_gui/corpora_reader_gui/static'), maxage=2629000)
favicon = views.Staticfile(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../corpora_reader_gui/corpora_reader_gui/static/favicon.ico'), maxage=2629000)

evwsgi.wsgi_cb(('/favicon.ico', favicon))
evwsgi.wsgi_cb(('/static', staticfile))


evwsgi.wsgi_cb(('',generic))
evwsgi.set_debug(0)
evwsgi.run()



