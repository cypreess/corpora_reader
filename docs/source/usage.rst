Usage
=====

Corpora reader comes with handy unix tool named ``corpora_reader.py`` which does all the magic and serves Web GUI.

::

    usage: corpora_reader.py [-h] [--port PORT] [--host HOST] corpus_path

    Creates URL seed set for focused crawler.

    positional arguments:
      corpus_path

    optional arguments:
      -h, --help   show this help message and exit
      --port PORT  defaults to 8000
      --host HOST  defaults to 127.0.0.1




Example:
--------
Yeah, that's really simple:
::

    $ corpora_reader.py /Users/dorosz/python_data/pap-45
    start on ('127.0.0.1', 8000)
    listen on 127.0.0.1:8000
    Using select as event backend

Navigate your web browser to http://localhost:8000/ and enjoy.