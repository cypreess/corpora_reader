.. Corpora Reader documentation master file, created by
   sphinx-quickstart on Fri Jun 22 11:34:16 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Corpora Reader's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 2

   introduction
   usage
   advanced

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

