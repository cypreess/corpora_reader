Advanced topics
===============

Reader index
-------------
Reader is a Django application. It uses internally a Sqlite3 database. Before starting a webserver ``corpora_reader.py`` checks
if ``reader.sqlite`` file exists in the corpus internal directory. If not - it creates it.

.. warning::

    Because of initial data indexing  first run can take a very long time. But as soon as index file is created
    reader will start immediately.

``CORPUS_PATH`` environment variable
-----------------------------------
``corpora_reader.py`` communicates with underneath Django application ``corpora_reader_gui`` with setting special environment
 variable named ``CORPUS_PATH`` which is set to a given by the user corpus path. In fact any usage of ``manage.py`` of this
 application need to be proceeded with this path::


    CORPUS_PATH=/some/path/to/corpus ./manage.py dbshell

.. warning::

    This usage is strictly hypothetical as you will never need to use it that way, unless you really know what you are doing.