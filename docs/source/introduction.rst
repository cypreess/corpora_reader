Introduction
============

Corpora Reader is a very fast Web based GUI for browsing corpora in `Corpora format <http://packages.python.org/Corpora/>`_.
One instance of the program can handle one corpus at the same time.


Installation
------------
Corpora reader is using `fapws3 <http://www.fapws.org/>`_ which has been reported to run with `libev`. Please ask your system administrator to install
  `libev` before proceeding with following instructions. You can find some details also on `fapws installation page <http://www.fapws.org/getting-started>`_.

To install Corpora Reader simply type::

    pip install -e git+https://cypreess@bitbucket.org/cypreess/corpora_reader.git

