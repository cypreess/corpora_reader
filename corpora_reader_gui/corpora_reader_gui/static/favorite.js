
function render_star( obj){
    if ($(obj).attr('favorite') == 'True') {
        $(obj).html('<img src="' + STATIC_URL + 'favorite_star_on.gif">');
    } else {
        $(obj).html('<img src="' + STATIC_URL + 'favorite_star_off.gif">');
    }
}


$(function(){
    $('.favorite').click(function(){
        if ($(this).attr('favorite') == 'True') {
            $(this).attr('favorite', 'False');
            render_star(this);
        } else{
            $(this).attr('favorite', 'True');
            render_star(this);
        }
        $.get('/favorite/' + $(this).attr('note') + '/' + $(this).attr('favorite') + '/' );

    });
    $('.favorite').each(function(){
        render_star(this);
    })
});