from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from corpora_reader_gui.viewer.views import NotesView, NoteView, FavoriteNote

urlpatterns = patterns('',
    # Examples:
    url(r'^$', NotesView.as_view(), name='notes'),
    url(r'^note/(?P<pk>[0-9]+)/$', NoteView.as_view(), name='note'),
    url(r'^favorite/(?P<pk>[0-9]+)/(?P<favorite>True|False)/$', FavoriteNote.as_view(), name='favorite'),

    # url(r'^corpora_reader_gui/', include('corpora_reader_gui.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
urlpatterns += staticfiles_urlpatterns()