# Create your views here.
from copy import deepcopy
from django.conf import settings
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from django.utils.translation import pgettext_lazy
from django.views.generic.base import View
from django.views.generic.detail import DetailView, SingleObjectMixin, BaseDetailView
from django.views.generic.list import ListView
from corpora_reader_gui.viewer.models import Note

class OrderFilterListView(ListView):
    """
    ListView allowing ordering and filtering listed model
    """
    order_columns = []
    "order_columns format is ({ 'column': 'column__name', 'name': 'visible name'} , ...)"

    order_types = [{ 'type': 'asc', 'name': 'ascending'}, { 'type': 'desc', 'name': 'descending'}]


    def get_query(self):
        return self.request.GET.get('query', '')

    def get_order_columns(self):
        return self.order_columns

    def get_order_types(self):
        return self.order_types


    def get_order_column(self):
        """
        Returns order column from following sources:
         * request arguments
         * session
         * default (first)
        """
        if self.request.GET.has_key('order_column') and self.request.GET['order_column'] in map(lambda c: c['column'],self.get_order_columns()):
            self.request.session['galleries_view_order_column'] = self.request.GET['order_column']
            return self.request.GET['order_column']
        elif self.request.session.has_key('galleries_view_order_column'):
            return self.request.session['galleries_view_order_column']
        else:
            return self.order_columns[0]['column']

    def get_order_type(self):
        """
        Returns order type from following sources:
         * request arguments
         * session
         * default (== asc)
        """
        if self.request.GET.has_key('order_type') and self.request.GET['order_type'] in map(lambda t: t['type'], self.get_order_types()):
            self.request.session['galleries_view_order_type'] = self.request.GET['order_type']
            return self.request.GET['order_type']
        elif self.request.session.has_key('galleries_view_order_type'):
            return self.request.session['galleries_view_order_type']
        else:
            return 'asc'

    def get_queryset_order_filter(self, order_column, order_type):
        """
        Returns string argument for order_by() QuerySet method
        """
        return ('-' if order_type == 'desc' else '') + order_column

    def get_query_filter_pk(self, query):
        raise NotImplementedError('Please hook here your indexing service and return list of model\'s pk matching query: %s' % query)

    def get_queryset(self):
        queryset = super(OrderFilterListView, self).get_queryset()
        if self.get_order_columns():
            queryset = queryset.order_by(
                self.get_queryset_order_filter(self.get_order_column(),
                self.get_order_type())
            )

        if self.get_query():
            queryset = queryset.filter(pk__in = self.get_query_filter_pk(self.get_query()))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderFilterListView, self).get_context_data(**kwargs)
        context['query'] = self.get_query()
        context['order_type'] = self.get_order_type()
        context['order_column'] = self.get_order_column()
        context['order_types'] = deepcopy(self.get_order_types())
        for x in context['order_types']:
            x['selected'] = x['type'] == context['order_type']
        context['order_columns'] = deepcopy(self.get_order_columns())
        for x in context['order_columns']:
            x['selected'] = x['column'] == context['order_column']
        return context



class NotesView(OrderFilterListView):
    model = Note
    paginate_by = 10
    order_columns = [   {'column': 'id', 'name': pgettext_lazy('order by', 'id')},
            {'column': 'timestamp', 'name': pgettext_lazy('order by', 'timestamp')},
            {'column': 'score', 'name': pgettext_lazy('order by', 'score')},


    ]
    order_types = [{ 'type': 'asc', 'name': mark_safe('&#x25B2;')}, { 'type': 'desc', 'name': mark_safe('&#x25BC;')}]
    def get_context_data(self, **kwargs):
        context = super(NotesView, self).get_context_data(**kwargs)
        context['favorite']=self.request.session.get('favorite', None)
        return context

    def get_queryset(self):
        if self.request.GET.get('favorite') == 'True':
            self.request.session['favorite'] = True
        elif self.request.GET.get('favorite') == 'False':
            self.request.session['favorite'] = None
        if self.request.session.get('favorite', None) is None:
            return super(NotesView, self).get_queryset()
        else:
            return super(NotesView, self).get_queryset().filter(favorite=self.request.session.get('favorite'))

class NoteView(DetailView):
    model = Note

    def get_context_data(self, **kwargs):
        context = super(NoteView, self).get_context_data(**kwargs)
        note = settings.CORPUS[self.object.corpus_id]
        context['fulltext'] = note[1]
        context['headers'] = note[0]
        return context


class FavoriteNote(BaseDetailView):
    model = Note

    def render_to_response(self, context):
        self.object.favorite = eval(self.kwargs['favorite'])
        self.object.save()
        return HttpResponse('OK')