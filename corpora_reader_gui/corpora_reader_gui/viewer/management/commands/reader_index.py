from datetime import datetime
from django.core import management
import os
import bsddb3
from corpora.corpus import Corpus
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from corpora_reader_gui.viewer.models import Note

def mygetattr(dict, key, default):
    try:
        return dict[key]
    except KeyError:
        return default

class Command(BaseCommand):
    args = '<corpus_path>'
    help = 'Creates index for corpora reader'

    def handle(self, *args, **options):
        try:
            path = args[0]
        except IndexError:
            raise CommandError('Please provide path to a corpus')
        self.stdout.write('Reading corpus "%s"\n' % path)

        try:
            corpus = Corpus(path)
        except bsddb3.db.DBNoSuchFileError:
            raise CommandError('Corpus not found at given path')

        settings.DATABASES['default']['NAME'] = os.path.join(path, settings.READER_INDEX_FILENAME)
        self.stdout.write("Creating %s database\n" % settings.DATABASES['default']['NAME'])
        management.call_command('syncdb', verbosity=0, interactive=False)
        management.call_command('flush', verbosity=0, interactive=False)

        for note in corpus:
            self.stdout.write('Processing %d - %s\n' % (note[0]['id'],note[0]['title']))
            Note(   corpus_id=note[0]['id'],
                    timestamp=mygetattr(note[0], 'timestamp', datetime.now()),
                    title=mygetattr(note[0], 'title', 'No title'),
                    snippet=note[1][0:200],
                    source=mygetattr(note[0], 'from', ''),
                    score=mygetattr(note[0], 'score', None),
                    ).save()

        self.stdout.write('Done\n')