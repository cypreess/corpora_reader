from django.db import models

# Create your models here.

class Note(models.Model):
    corpus_id = models.TextField()
    timestamp = models.DateTimeField(db_index=True)
    title = models.TextField(blank=True)
    snippet = models.TextField(blank=True)
    source = models.TextField(blank=True)
    favorite = models.BooleanField(default=False, db_index=True)
    score = models.IntegerField(default=0, db_index=True, null=True, blank=True)
    def __unicode__(self):
        return u'Note %s' % self.corpus_id