import os
from django.conf import settings

def corpora_name(request):
    return {
        'corpus_name' : os.path.basename(settings.CORPUS_PATH),
        'corpus_path': settings.CORPUS_PATH,
        }